import functools
import json
import logging
import os
import sys
import time
import traceback
from pathlib import Path
from typing import Callable, List, Optional, Sequence, Tuple, Union

import h5py
import numpy as np
import yaml
from termcolor import colored


def Print(*message, color=None, on_color=None, sep=" ", end="\n", verbose=True):
    """
    Print function integrated with color.
    """
    if verbose:
        color_map = {
            "r": "red",
            "g": "green",
            "b": "blue",
            "y": "yellow",
            "m": "magenta",
            "c": "cyan",
            "w": "white",
        }
        if color is None:
            print(*message, end=end)
        else:
            color = color_map[color] if len(color) == 1 else color
            print(
                colored(sep.join(map(str, message)), color=color, on_color=on_color),
                end=end,
            )


def check_dir(*arg, isFile=False, exist_ok=True):
    path = Path(*arg)
    if isFile:
        filename = path.name
        path = path.parent

    if not path.is_dir():
        os.makedirs(path, exist_ok=exist_ok)
    return path / filename if isFile else path


def get_items_from_file(filelist, format="auto", sep="\n"):
    """
    Simple wrapper for reading items from file.
    If file is dumped by yaml or json, set `format` to `json`/`yaml`.
    """
    filelist = Path(filelist)
    if not filelist.is_file():
        raise FileNotFoundError(f"No such file: {filelist}")

    if format == "auto":
        if filelist.suffix in [".json"]:
            format = "json"
        elif filelist.suffix in [".yaml", ".yml"]:
            format = "yaml"
        else:
            format = None

    with filelist.open() as f:
        if format == "yaml":
            lines = yaml.full_load(f)
        elif format == "json":
            lines = json.load(f)
        else:
            lines = f.read().split(sep)
    return lines


def load_h5(h5_file: str, keywords: list, transpose=None, verbose=False):
    hf = h5py.File(h5_file, "r")
    # print('List of arrays in this file: \n', hf.keys())
    if len(keywords) == 0:
        Print("Get all items:", hf.keys(), verbose=verbose)
        keywords = list(hf.keys())

    dataset = [np.copy(hf.get(key)) if key in hf.keys() else None for key in keywords]
    if verbose:
        [
            Print(f"{key} shape: {np.shape(data)}", color="g")
            if data is not None
            else Print(f"{key} is None", color="r")
            for key, data in zip(keywords, dataset)
        ]

    if transpose:
        dataset = [
            np.transpose(data, transpose) if data is not None else None
            for data in dataset
        ]
    return dataset


def recursive_glob(searchroot=".", searchstr="", verbose=False):
    """
    recursive glob with one search keyword
    """
    if not os.path.isdir(searchroot):
        raise ValueError(f"No such directory: {searchroot}")

    if "*" not in searchstr:
        searchstr = "*" + searchstr + "*"

    Print(f"search for {searchstr} in {searchroot}", verbose=verbose)

    f = [
        Path(rootdir).joinpath(filename)
        for rootdir, dirnames, filenames in os.walk(searchroot)
        for filename in filenames
        if Path(filename).match(searchstr)
    ]
    f.sort()
    return f


def recursive_glob2(
    searchroot: str,
    searchstr_list: Union[List, Tuple],
    logic: str = "and",
    verbose: bool = False,
):
    """
    recursive glob with multiple search keywords
    """
    if not os.path.isdir(searchroot):
        raise ValueError(f"No such directory: {searchroot}")
    if searchstr_list is None or len(searchstr_list) == 0:
        raise ValueError(f"Incorrect searchstr_list is given: {searchstr_list}")

    if logic == "and":
        logic_op: Callable = np.all
    elif logic == "or":
        logic_op: Callable = np.any

    searchstr_list = [
        "*" + searchstr + "*" if "*" not in searchstr else searchstr
        for searchstr in searchstr_list
    ]

    Print(f"search for {searchstr_list} in {searchroot}", verbose=verbose)

    f = [
        Path(rootdir).joinpath(filename)
        for rootdir, dirnames, filenames in os.walk(searchroot)
        for filename in filenames
        if logic_op([Path(filename).match(ss) for ss in searchstr_list])
    ]
    f.sort()
    return f


#! Todo: add logic_op option like recursive_glob2
def recursive_glob3(
    searchroot: str,
    searchstr_list: Union[List, Tuple],
    excludestr_list: Optional[Union[List, Tuple]] = None,
    searchstr_op: str = "or",
    excludestr_op: str = "or",
    verbose=False,
):
    """
    searchroot: search root dir.
    searchstr_list: search keywords list. [required]
    excludestr_list: not search keywords list. [optional]
    """
    if not os.path.isdir(searchroot):
        raise ValueError(f"No such directory: {searchroot}")

    if searchstr_list is None:
        raise ValueError("Search keyword list is empty!")

    Print(
        "search for:",
        searchstr_list,
        "exclude:",
        excludestr_list,
        "in:",
        searchroot,
        verbose=verbose,
    )

    if excludestr_op == "and":
        ex_logic_op: Callable = np.all
    elif excludestr_op == "or":
        ex_logic_op: Callable = np.any

    files = recursive_glob2(
        searchroot, searchstr_list, logic=searchstr_op, verbose=verbose
    )

    excludestr_list = [
        "*" + exclu_str + "*" if "*" not in exclu_str else exclu_str
        for exclu_str in excludestr_list
    ]

    if excludestr_list is not None:
        files = [
            fname
            for fname in files
            if not ex_logic_op([Path(fname).match(ss) for ss in excludestr_list])
        ]

    files.sort()
    return files


def PrintProgressBar(
    iteration,
    total,
    prefix="",
    suffix="",
    decimals=1,
    length=100,
    fill="=",
    display=True,
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    if display:
        iteration = iteration + 1
        percent = ("{0:." + str(decimals) + "f}").format(
            100 * (iteration / float(total))
        )
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + "-" * (length - filledLength)
        print("\r%s [%s] %s%% %s" % (prefix, bar, percent, suffix), end="\r")
        # Print New Line on Complete
        if iteration == total:
            print()


def save_sourcecode(code_rootdir, out_dir, file_type="*.py", verbose=True):
    if not os.path.isdir(code_rootdir):
        raise FileNotFoundError(f"Code root dir not exists! {code_rootdir}")

    Print("Backup source code under root_dir:", code_rootdir, color="y", verbose=verbose)
    outpath = check_dir(out_dir, f"{os.path.basename(code_rootdir)}_{time.strftime('%m%d_%H%M')}.tar", isFile=True)
    tar_opt = "cvf" if verbose else "cf"
    os.system(f"find {code_rootdir} -name '{file_type}' | tar -{tar_opt} {outpath} -T -")


def plot_confusion_matrix(
    y_true,
    y_pred,
    filename,
    labels,
    ymap=None,
    figsize=(10, 10),
    true_axis_label="Ground-truth",
    pred_axis_label="Preditions",
):
    """
    Generate matrix plot of confusion matrix with pretty annotations.
    The plot image is saved to disk.
    args:
      y_true:    true label of the data, with shape (nsamples,)
      y_pred:    prediction of the data, with shape (nsamples,)
      filename:  filename of figure file to save
      labels:    string array, name the order of class labels in the confusion matrix.
                 use `clf.classes_` if using scikit-learn models. with shape (nclass,).
      ymap:      dict: any -> string, length == nclass.
                 if not None, map the labels & ys to more understandable strings.
                 Caution: original y_true, y_pred and labels must align.
      figsize:   the size of the figure plotted.
    """
    import matplotlib
    import numpy as np
    import pandas as pd

    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import seaborn as sns
    from sklearn.metrics import confusion_matrix

    if ymap is not None:
        y_pred = [ymap[yi] for yi in y_pred]
        y_true = [ymap[yi] for yi in y_true]
        labels = [ymap[yi] for yi in labels]
    cm = confusion_matrix(y_true, y_pred, labels=labels)
    cm_sum = np.sum(cm, axis=1, keepdims=True)
    cm_perc = cm / cm_sum * 100
    annot = np.empty_like(cm).astype(str)
    nrows, ncols = cm.shape
    for i in range(nrows):
        for j in range(ncols):
            c = cm[i, j]
            p = cm_perc[i, j]
            if i == j:
                s = cm_sum[i]
                annot[i, j] = "%.1f%%\n%d/%d" % (p, c, s)
            elif c == 0:
                annot[i, j] = ""
            else:
                annot[i, j] = "%.1f%%\n%d" % (p, c)
    cm = pd.DataFrame(cm, index=labels, columns=labels)
    cm.index.name = true_axis_label
    cm.columns.name = pred_axis_label
    fig, ax = plt.subplots(figsize=figsize)
    sns.heatmap(cm, annot=annot, fmt="", ax=ax)
    plt.savefig(filename)


def split_train_test(
    datalist: list,
    test_ratio: float,
    label_key: str = "label",
    splits_num: int = 1,
    output_dir: Optional[str] = None,
    output_prefix: Optional[str] = "",
    stratify: bool = True,
    random_seed: int = 0,
    verbose=False,
):
    if stratify:
        from sklearn.model_selection import StratifiedShuffleSplit as Split
    else:
        from sklearn.model_selection import ShuffleSplit as Split

    if test_ratio <= 0 or 1 <= test_ratio:
        raise ValueError(f"Expect: 0 < Test ratio < 1, but got {test_ratio}")

    if isinstance(datalist[0][label_key], (list, tuple)):
        Ys = np.array([np.mean(data[label_key]) for data in datalist])
    else:
        Ys = np.array([int(data[label_key]) for data in datalist])

    output = []
    spliter = Split(n_splits=splits_num, test_size=test_ratio, random_state=random_seed)
    for i, (train_idx, test_idx) in enumerate(spliter.split(np.zeros(len(Ys)), Ys)):
        Print("Train_idx: ", train_idx, verbose=verbose)
        Print("Test_idx:", test_idx, verbose=verbose)

        train_cohort = [datalist[idx] for idx in train_idx]
        test_cohort = [datalist[idx] for idx in test_idx]
        output.append((train_cohort, test_cohort))

        if output_dir and os.path.isdir(output_dir):
            with open(
                os.path.join(
                    output_dir, f"{output_prefix}_train_cohort{i}_{test_ratio}.json"
                ),
                "w",
            ) as f:
                json.dump(train_cohort, f, indent=2)
            with open(
                os.path.join(
                    output_dir, f"{output_prefix}_test_cohort{i}_{test_ratio}.json"
                ),
                "w",
            ) as f:
                json.dump(test_cohort, f, indent=2)

    return output


def catch_exception(
    handled_exception_type: Exception,
    logger_name: Optional[str] = None,
    show_args: bool = True,
    show_details: bool = False,
    return_none: bool = False,
    path_keywords: Union[Sequence[str], str] = [],
):
    """Try catch exception function, help to better show error msg.

    Args:
        handled_exception_type (Exception): own exception type that have handled.
        logger_name (Option[str], optional): logger name. Defaults to None.
        show_args (bool, optional): whether to show the input args of error func. Defaults to True.
        show_details (bool, optional): wheter to show the original exception msg. Defaults to False.
        return_none (bool, optional): if true return None, otherwise exit. Defaults to False.
        path_keywords (Union[str, Sequence[str]]): keywords of own code path, help to locate key func.
    """
    import re
    file_regex = r"(\/.*?\.[\w:]+)"
    
    def _get_related_error_msg(tb):
        error_msg = None
        keywords = path_keywords if isinstance(path_keywords, (tuple, list)) else [path_keywords, ]
        for line in reversed(traceback.format_tb(tb)):
            file_path = re.search(file_regex, line).group(1).lower()
            if any([keyword.lower() in file_path for keyword in keywords]):
                error_msg = line
                break
        return error_msg

    def wrapper(func):
        @functools.wraps(func)
        def inner_wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except handled_exception_type as e:
                log_info = \
                  f"\n{'! '*30}\n{colored('Error occurred! Please check your code! Msg:', color='red')} {e}\n\n"
                while hasattr(e, "__cause__") and e.__cause__:
                    log_info += f"- {''.join(traceback.format_tb(e.__traceback__, limit=-2)[0])}"
                    log_info += f"→ {e.__cause__}\n\n"
                    e = e.__cause__
                log_info += f"{'! '*30}\n"

                if logger_name is None:
                    print(log_info)
                else:
                    logger = logging.getLogger(logger_name)
                    logger.exception(log_info)

                if return_none:
                    return
                else:
                    sys.exit(-1)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                Print("Exception trace:", color="r")
                Print("Exception entry is:", color='y')
                print(" ".join(traceback.format_tb(exc_tb, limit=2)[-1:]))
                if show_args:
                    print(colored("    ↑ Input arguments:", color='cyan'), args, kwargs)

                if path_keywords:
                    error_msg = _get_related_error_msg(exc_tb)
                else:
                    error_msg = " ".join(traceback.format_tb(exc_tb, limit=-1))

                Print("Exception occured at:", color='y')
                print(error_msg)
                print(traceback.format_exc().splitlines()[-1], '\n')

                if show_details:
                    print(traceback.format_exc())

                if return_none:
                    return
                else:
                    sys.exit(-1)

        return inner_wrapper

    return wrapper
